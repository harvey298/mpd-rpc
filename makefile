p=mpdrpc
main:
	pyinstaller --clean --specpath build/specs -n $p -F src/main.py

clean:
	rm -r build/$p
	rm -r src/__pycache__

tclean:
	rm -r build/
	rm -r dist
	rm -r src/__pycache__
	rm -rf colour-lib

git:
	git pull

libs:
	git clone https://github.com/harvey298/colour-lib
	mv colour-lib/src/main.py src/colourlib.py
	rm -rf colour-lib

install:
	make main
	sudo cp dist/mpdrpc /usr/bin/

uninstall:
	sudo rm /usr/bin/mpdrpc