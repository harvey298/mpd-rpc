# mpd-rpc
[![pypresence](https://img.shields.io/badge/using-pypresence-00bb88.svg?style=for-the-badge&logo=discord&logoWidth=20)](https://github.com/qwertyquerty/pypresence)

## Overview
[mpd-rpc](https://gitlab.com/garrhow/mpd-rpc) is a program to display your current song that is playing in MPD, so you can look cool while listening to your favorite song

## Table of Contents
1.[Install](#install)
    - [Prerequisites](#Prerequisites)
    - [Make Instructions](#Make-Instructions)
2. [Useage](#useage)

## Install

### Prerequisites

 - [Git](https://www.nodejs.org/en/download/)
 - [Python 3.7+](https://www.python.org/downloads/)
 - [Pyinstaller](https://www.pyinstaller.org/) (make)
 - [pypresence](https://pypi.org/project/pypresence/) (make/source use)

### Make Instructions
if you want to update the libarys that aren't on pip run
```
make libs
```
to note one thing, the caution libary does not currently have its own repositry!


if you just wish to just build mpd-rpc run
```
make main
```

if you want to install and build mpd-rpc run
```
make install
```

## Useage
if you just want to use it from source you can run
```
python3 main.py
```

if you installed it you can run
```
mpdrpc
```

it doesn't need any arguments!
