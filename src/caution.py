# 
# Copyright harvey298 2021 GPL
#
from colourlib import merge_format, format_reset
from util import empty_code
import sys
def caution(warning):
    print(merge_format("bold", "yellow")+"[Cauiton] "+format_reset(2)+warning+format_reset(0))

def error(error,status):
    print(merge_format("bold", "red")+"[ERROR] "+format_reset(2)+str(error)+format_reset(0))
    if status == 0:
        empty_code("e")
    else:
        sys.exit(status)

def info(info):
    print(merge_format("bold", "blue")+"[INFO] "+format_reset(2)+str(info)+format_reset(0))