#!/usr/bin/env python

from pypresence import Presence
from colourlib import merge_format, format_reset
from caution import caution, error, info
from threading import Thread
import musicpd, time, sys

clientid = "852906320121430016"

rpc = Presence(clientid)

def main():
    rpc.connect()
    try:
        mpc = musicpd.MPDClient()
        info("Welcome to mpd-rpc by Howard")
        info("Repo: https://gitlab.com/garrhow/mpd-rpc\n")
        while True:
            try:
                mpc.connect()
            except ConnectionRefusedError as e:
                error(e,0)
                error("mpd doesn't seem to be running! Exiting!",1)
            song = mpc.currentsong()
            if song == {}:
                info(merge_format("bold","red")+"No Song Playing! "+format_reset(0))
                info("Waiting!")
                while song == {}:
                    song = mpc.currentsong()
                    if song != {}:
                        info(merge_format("bold","green")+"Song Playing!"+format_reset(0))
                    time.sleep(1)
            else:
                try:
                    rpc.update(details=('Listening to '+song['title']), state=('By '+song['artist']+', on '+song['album']))
                except KeyError as e:
                    caution("Falling back to file name id")
                    rpc.update(details=('Listening to '+song['file']), state=("Not provided!"))
            time.sleep(3)
            mpc.disconnect()
    except KeyboardInterrupt:
        mpc.disconnect()
        print("\n")
        info("Ciao!")
        

if __name__ == '__main__':
    main()